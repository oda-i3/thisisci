require 'rspec'

describe 'Arithmetic' do
  describe 'Addition' do
    it '1 + 1 is 2' do
      expect(1 + 1).to eq 2
    end
  end
  describe 'Subtraction' do
    it '10 - 1 is 9' do
      expect(10 - 1).to eq 9
    end
  end
end
