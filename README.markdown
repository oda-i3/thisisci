Continuous Integration Festival
----
![songkran](songkran-festival.jpg)



TODO
----
- Tour the source code. Examine the build and reporting in guard and Bitbucket Pipelines. Look at the steps, lint reports, tests and reports.

- Try making the build fail by pushing your own branch with changes. How does the CI build respond, and how can that help us and our teams? What happens to the quality of reporting if the build fails part way, and how can we improve it?

- Add [simplecov](https://github.com/colszowka/simplecov) to bitbucket-pipeline, and, if possible, to guard.

- Add a new class and corresponding test suite. First, try committing failing tests or no tests at all, and observe the impact on the build and the coverage metrics. Then commit fixes so that all tests will pass and coverage rates are restored.

- Change build to execute only when actual code changes. For example, a new build should not be started whenever only ```README.markdown``` was changed.

- Add Internationalization (I18n) and Localization (L10n) resources and some strings. Exercise the API in the ```lib/``` classes. Add I18n checking to the build using [i18n-tasks](https://github.com/glebm/i18n-tasks).

- Add the following build steps to both bitbucket-pipeline and guard:
    - [metric-fu](https://github.com/metricfu/metric_fu/)
    - [ruby-lint](https://gitlab.com/yorickpeterse/ruby-lint)

- Recreate the build in Rake. Question: What is the difference between a dedicated build step, and the idioms provided by the build system?

- In addition to Bitbucket Pipelines, let's try running the build on these CI systems:
    - [Atlassian Bamboo](https://www.atlassian.com/software/bamboo)
    - [Circle CI](https://circleci.com/)
    - [Codeship](https://codeship.com/)
    - [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)
    - [Jenkins](https://bitbucket.org/ty-i3/infra)
    - [JetBrains TeamCity](https://www.jetbrains.com/teamcity/)
    - Microsoft VSTS (Part of our MSDN subscription)
    - [Travis CI](https://travis-ci.org/)
